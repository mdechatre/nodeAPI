/* jshint esversion: 6 */

const path = require('path');
const os = require('os');
const fs = require('fs');

var pathObj = path.parse(__filename);

var totalMemory = os.totalmem();
var freeMemory = os.freemem();
var osType = os.type();

console.log(pathObj);

console.log('Total Memory: ' + totalMemory);
console.log(`Free Memory: ${freeMemory}`);
console.log(`Type OS: ${osType}`);